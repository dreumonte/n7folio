---
title: "International Mobility"
#image: 
slug: "mobility"
style:
    background: "#8fbcbb"
    color: "#eceff4"

displayAsArticle: true
readingTime: false

menu:
  main:
    name: International Mobility
    weight: 300
    params:
      icon: earth
---

I haven't yet completed my mobility, but I'm planning to do my final intership in a foreign country. I am looking for a company that will allow me to develop my skills in the field of infrastructure and development. I am particularly interested in the fields of cybersecurity, DevOps and cloud. You can find further information in my [PPP](/ppp.pdf)